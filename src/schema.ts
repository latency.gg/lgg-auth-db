import * as fs from "fs";
import * as path from "path";
import { projectRoot } from "./utils/index.js";

export function getSchema() {
    const schemaSql = fs.readFileSync(path.join(projectRoot, "out", "schema.sql"), "utf8");
    return schemaSql;
}
