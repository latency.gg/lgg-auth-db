import pg from "pg";
import { RowDescriptor, RowFilter } from "table-access";
import { createRowEvents, getTriggerEventsFanoutMemoizer, RowEventUnion } from "../utils/index.js";

const schema = "public";
const table = "user";

export interface UserRowKey {
    id: string
}
export interface UserRowData {
    name: string
    salt: string
    created_utc: string
}
export type UserRow = UserRowKey & UserRowData
export const userRowDescriptor: RowDescriptor<UserRow> = {
    schema, table,
};
export type UserRowEvent = RowEventUnion<UserRowKey, UserRowData>;

export function createUserRowEvents(
    pool: pg.Pool,
    filter: Partial<UserRowKey> | RowFilter<UserRowKey>,
    onError: (error: unknown) => void,
    signal: AbortSignal,
): AsyncIterable<UserRowEvent> {
    const triggerEventsFanoutMemoizer = getTriggerEventsFanoutMemoizer(pool, onError);

    return createRowEvents<UserRowKey, UserRowData>({
        channel: "row-event",
        schema,
        table,
        filter,
        pool,
        signal,
        triggerEventsFanoutMemoizer,
    });
}
