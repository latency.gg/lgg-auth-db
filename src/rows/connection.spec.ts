import { isAbortError } from "abort-tools";
import assert from "assert";
import * as pg from "pg";
import { queryDelete, queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { connectionRowDescriptor, createConnectionRowEvents } from "./connection.js";
import { userRowDescriptor } from "./user.js";

test("connection", t => withContext(async context => {
    await withTransaction(context.pool, initializeMocks);

    const abortController = new AbortController();
    const eventIterable = createConnectionRowEvents(
        context.pool,
        {},
        error => t.ok(isAbortError(error), "expected AbortError"),
        abortController.signal,
    );
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {
        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "snapshot");
            t.deepEqual(
                eventNext.value.rows,
                [
                ],
            );
        }

        await withTransaction(context.pool, async connection => {
            await queryInsert(connection,
                connectionRowDescriptor,
                {
                    provider: "dummy",
                    subject: "subby",
                    user_id: "\\xaa",
                    created_utc: "2022-04-12T11:05:05",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "insert");
            t.deepEqual(
                eventNext.value.key,
                {
                    provider: "dummy",
                    subject: "subby",
                    user_id: "\\xaa",
                },
            );
            t.deepEqual(
                eventNext.value.data,
                {
                    created_utc: "2022-04-12T11:05:05",
                },
            );
        }

        await withTransaction(context.pool, async connection => {
            await queryDelete(connection,
                connectionRowDescriptor,
                {
                    provider: "dummy",
                    subject: "subby",
                    user_id: "\\xaa",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "delete");
            t.deepEqual(
                eventNext.value.key,
                {
                    provider: "dummy",
                    subject: "subby",
                    user_id: "\\xaa",
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.ok(isAbortError(error), "expected AbortError"),
        );
    }
}));

async function initializeMocks(client: pg.ClientBase) {
    await queryInsert(client, userRowDescriptor, {
        id: "\\xaa",
        name: "testy",
        created_utc: "2022-04-12T11:01:45",
        salt: "\\xBB",
    });
}
