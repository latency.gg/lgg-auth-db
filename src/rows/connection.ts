import pg from "pg";
import { RowDescriptor, RowFilter } from "table-access";
import { createRowEvents, getTriggerEventsFanoutMemoizer, RowEventUnion } from "../utils/index.js";

const schema = "public";
const table = "connection";

export interface ConnectionRowKey {
    provider: string
    subject: string
    user_id: string
}
export interface ConnectionRowData {
    created_utc: string
}
export type ConnectionRow = ConnectionRowKey & ConnectionRowData
export const connectionRowDescriptor: RowDescriptor<ConnectionRow> = {
    schema, table,
};
export type ConnectionRowEvent = RowEventUnion<ConnectionRowKey, ConnectionRowData>;

export function createConnectionRowEvents(
    pool: pg.Pool,
    filter: Partial<ConnectionRowKey> | RowFilter<ConnectionRowKey>,
    onError: (error: unknown) => void,
    signal: AbortSignal,
): AsyncIterable<ConnectionRowEvent> {
    const triggerEventsFanoutMemoizer = getTriggerEventsFanoutMemoizer(pool, onError);

    return createRowEvents<ConnectionRowKey, ConnectionRowData>({
        channel: "row-event",
        schema,
        table,
        filter,
        pool,
        signal,
        triggerEventsFanoutMemoizer,
    });
}
