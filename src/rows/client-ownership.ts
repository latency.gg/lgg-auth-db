import pg from "pg";
import { RowDescriptor, RowFilter } from "table-access";
import { createRowEvents, getTriggerEventsFanoutMemoizer, RowEventUnion } from "../utils/index.js";

const schema = "public";
const table = "client_ownership";

export interface ClientOwnershipRowKey {
    user_id: string
    client_id: string
}
export interface ClientOwnershipRowData {
    created_utc: string
}
export type ClientOwnershipRow = ClientOwnershipRowKey & ClientOwnershipRowData
export const clientOwnershipRowDescriptor: RowDescriptor<ClientOwnershipRow> = {
    schema, table,
};
export type ClientOwnershipRowEvent = RowEventUnion<ClientOwnershipRowKey, ClientOwnershipRowData>;

export function createClientOwnershipRowEvents(
    pool: pg.Pool,
    filter: Partial<ClientOwnershipRowKey> | RowFilter<ClientOwnershipRowKey>,
    onError: (error: unknown) => void,
    signal: AbortSignal,
): AsyncIterable<ClientOwnershipRowEvent> {
    const triggerEventsFanoutMemoizer = getTriggerEventsFanoutMemoizer(pool, onError);

    return createRowEvents<ClientOwnershipRowKey, ClientOwnershipRowData>({
        channel: "row-event",
        schema,
        table,
        filter,
        pool,
        signal,
        triggerEventsFanoutMemoizer,
    });
}
