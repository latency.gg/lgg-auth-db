import { isAbortError } from "abort-tools";
import assert from "assert";
import { queryDelete, queryInsert, queryUpdate, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { createUserRowEvents, userRowDescriptor } from "./user.js";

test("user", t => withContext(async context => {
    const abortController = new AbortController();
    const eventIterable = createUserRowEvents(
        context.pool,
        {},
        error => t.ok(isAbortError(error), "expected AbortError"),
        abortController.signal,
    );
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {
        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "snapshot");
            t.deepEqual(
                eventNext.value.rows,
                [
                ],
            );
        }

        await withTransaction(context.pool, async user => {
            await queryInsert(user,
                userRowDescriptor,
                {
                    id: "\\x32",
                    name: "testing",
                    salt: "\\x33",
                    created_utc: "2022-03-26T08:04:19",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "insert");
            t.deepEqual(
                eventNext.value.key,
                {
                    id: "\\x32",
                },
            );
            t.deepEqual(
                eventNext.value.data,
                {
                    name: "testing",
                    salt: "\\x33",
                    created_utc: "2022-03-26T08:04:19",
                },
            );
        }

        await withTransaction(context.pool, async user => {
            await queryUpdate(user,
                userRowDescriptor,
                {
                    id: "\\x32",
                },
                {
                    name: "testing-123",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "update");
            t.deepEqual(
                eventNext.value.key,
                {
                    id: "\\x32",
                },
            );
            t.deepEqual(
                eventNext.value.data,
                {
                    name: "testing-123",
                    salt: "\\x33",
                    created_utc: "2022-03-26T08:04:19",
                },
            );
        }

        await withTransaction(context.pool, async user => {
            await queryDelete(user,
                userRowDescriptor,
                {
                    id: "\\x32",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "delete");
            t.deepEqual(
                eventNext.value.key,
                {
                    id: "\\x32",
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.ok(isAbortError(error), "expected AbortError"),
        );
    }

}));
