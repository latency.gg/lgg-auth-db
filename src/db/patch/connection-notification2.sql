--@require connection-notification

drop trigger connection_row_event_trg_all
on public.connection;

drop function public.connection_row_event;

perform public.create_row_event_trigger(
    'public',
    'connection_row_event',
    'row-event',
    array['provider', 'subject', 'user_id'],
    array['created_utc']
);

create trigger connection_row_event_trg_all after
insert or update or delete
on public.connection
for each row
execute procedure public.connection_row_event()
;
