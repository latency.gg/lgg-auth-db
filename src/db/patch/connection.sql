--@require initial

create table public.connection(
    provider text not null,
    subject text not null,
    user_id bytea not NULL
);

alter table public.connection
add CONSTRAINT connection_pk PRIMARY KEY (provider, subject),
add constraint connection_user_fk FOREIGN KEY (user_id)
    REFERENCES public.user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
;
