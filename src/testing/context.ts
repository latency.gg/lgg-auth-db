import pg from "pg";
import { getSchema } from "../schema.js";

export interface Context {
    pool: pg.Pool
}

export async function withContext(job: (context: Context) => Promise<void>) {
    const schemaSql = getSchema();

    const dbName = `db_${new Date().valueOf()}`;
    const client = new pg.Client({});
    await client.connect();
    try {
        await client.query(`CREATE DATABASE ${client.escapeIdentifier(dbName)};`);
        try {
            const pool = new pg.Pool({
                database: dbName,
            });
            await pool.query(schemaSql);
            try {
                await job({ pool });
            }
            finally {
                await pool.end();
            }

        }
        finally {
            await client.query(`DROP DATABASE ${client.escapeIdentifier(dbName)};`);
        }
    }
    finally {
        await client.end();
    }

}
