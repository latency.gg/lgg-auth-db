import { second } from "msecs";

export const defaultSettings = {
    maxChunkAge: 10 * second,
    keepaliveInterval: 10 * second,
    wrapTreshold: 1000,
};
