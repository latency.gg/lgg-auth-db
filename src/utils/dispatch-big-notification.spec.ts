import delay from "delay";
import { second } from "msecs";
import pg from "pg";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("big-notification", t => withContext(async context => {
    const channel = "ch-123";
    const message = "0123456789";

    const client = await context.pool.connect();
    try {
        let notifications: Array<pg.Notification>;
        client.addListener(
            "notification",
            notification => notifications.push(notification),
        );
        await client.query(`
LISTEN ${client.escapeIdentifier(channel)};
`);

        {
            const chunkSize = message.length;
            notifications = [];

            await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

            await delay(2 * second);

            t.equal(notifications.length, 1);
            t.equal(notifications[0].payload, `1\n0\n${message}`);
        }

        {
            const chunkSize = message.length - 1;
            notifications = [];

            await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

            await delay(2 * second);

            t.equal(notifications.length, 2);
            t.equal(notifications[0].payload, `2\n1\n${message.substring(0 * chunkSize, 1 * chunkSize)}`);
            t.equal(notifications[1].payload, `2\n0\n${message.substring(1 * chunkSize)}`);
        }

        {
            const chunkSize = 4;
            notifications = [];

            await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

            await delay(2 * second);

            t.equal(notifications.length, 3);
            t.equal(notifications[0].payload, `3\n2\n${message.substring(0 * chunkSize, 1 * chunkSize)}`);
            t.equal(notifications[1].payload, `3\n1\n${message.substring(1 * chunkSize, 2 * chunkSize)}`);
            t.equal(notifications[2].payload, `3\n0\n${message.substring(2 * chunkSize)}`);
        }

        client.release();
    }
    catch (error) {
        client.release(true);
        throw error;
    }
}));
