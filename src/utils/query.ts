import { assertAbortSignal, setupCascadingAbort } from "abort-tools";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import { createIterableFanout } from "iterable-fanout";
import defer from "p-defer";
import { createRetryIterable } from "retry-iterable";
import { ErrorEvent, isErrorEvent } from "./error.js";

export interface QueryFactoryOptions<
    S = any, E = any, A extends unknown[] = any[]
    > {
    initialState: S,
    reduce: (state: S, event: E | ErrorEvent) => S,
    settle: (state: S, event: E | ErrorEvent) => boolean,
    createSource: (signal: AbortSignal, ...args: A) => AsyncIterable<E>,
    calculateHash: (...args: A) => string,
    onError: (error: unknown) => void
    retryIntervalBase: number,
    retryIntervalCap: number,
}

export type QueryFactory<
    S = any, E = any, A extends unknown[] = any[]
    > = InstanceMemoizer<
        Promise<QuerySource<S, E>>, A
    >

export function createQueryFactory<
    S = any, E = any, A extends unknown[] = any[]
>(
    options: QueryFactoryOptions<S, E, A>,
): QueryFactory<S, E, A> {
    const {
        initialState, reduce, settle,
        createSource, calculateHash, onError,
        retryIntervalBase, retryIntervalCap,
    } = options;

    const abortControllers = new WeakMap<
        Promise<QuerySource<S, E | ErrorEvent>>,
        AbortController
    >();

    const createInstace = (...args: A) => {
        const abortController = new AbortController();
        const instance = instantiate(
            abortController.signal,
            ...args,
        );
        abortControllers.set(instance, abortController);
        abortController.signal.addEventListener(
            "abort",
            () => abortControllers.delete(instance),
        );
        return instance;
    };

    const destroyInstance = (
        instance: Promise<QuerySource<S, E>>,
    ) => {
        const abortController = abortControllers.get(instance);
        assert(abortController);
        abortController.abort();
    };

    const result = new InstanceMemoizer(
        createInstace,
        destroyInstance,
        calculateHash,
    );
    return result;

    async function instantiate(
        signal: AbortSignal,
        ...args: A
    ) {
        const deferred = defer<void>();
        let settled = false;

        let state = initialState;

        const events = createRetryIterable<E | ErrorEvent>({
            signal,
            retryIntervalBase, retryIntervalCap,
            async * factory() {
                const abortController = new AbortController();
                setupCascadingAbort(signal, abortController);
                try {
                    const events = createSource(abortController.signal, ...args);
                    yield* events;
                    assertAbortSignal(signal, "query aborted");
                }
                finally {
                    abortController.abort();
                }
            },
            * mapError(error) {
                if (signal.aborted) return;

                onError(error);

                // If an error happend the retry logic will kick in and retry
                // the source. In some cases is might take a while for the error
                // to be resolved. For instance if there is a networking problem.
                // We yield an error event so that the query does not hang. Or
                // we can choose to make it hang via the settle function.
                yield { error: true };
            },
        });

        const eventsWithState = reduceEvents(events);

        const fanout = createIterableFanout(eventsWithState);

        const fork = fanout.fork;
        const getState = () => state;

        const querySource: QuerySource<S, E> = {
            getState,
            fork,
        };

        fanout.finished.catch(error => {
            // this will panic! but should never happen
            if (!signal.aborted) throw error;
        });

        await deferred.promise;

        return querySource;

        async function* reduceEvents(
            events: AsyncIterable<E | ErrorEvent>,
        ): AsyncIterable<QuerySourceElement<S, E>> {
            for await (const event of events) {
                state = reduce(state, event);

                yield { state, event };

                if (!settled && settle(state, event)) {
                    deferred.resolve();
                    settled = true;
                }
            }
        }
    }
}

export async function getQueryState<
    S = any, E = any, A extends unknown[] = any[]
>(
    source: InstanceMemoizer<Promise<QuerySource<S, E>>, A>,
    args: A,
    linger: number,
): Promise<S> {
    const queryPromise = source.acquire(...args);
    try {
        const query = await queryPromise;
        const state = query.getState();
        return state;
    }
    finally {
        source.release(queryPromise, linger);
    }
}

export interface QuerySourceElement<S, E> {
    state: S
    event: E | ErrorEvent
}

export interface QuerySource<S, E> {
    fork(signal?: AbortSignal): AsyncIterable<QuerySourceElement<S, E>>;
    getState(): S;
}

export async function* mapQueryEvents<E>(
    iterable: AsyncIterable<QuerySourceElement<unknown, E>>,
): AsyncIterable<E> {
    for await (const element of iterable) {
        // we dont want to expose errors to the world
        if (isErrorEvent(element.event)) continue;

        yield element.event;
    }
}

