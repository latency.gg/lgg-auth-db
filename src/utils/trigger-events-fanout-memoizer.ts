import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import { createIterableFanout, IterableFanout } from "iterable-fanout";
import * as pg from "pg";
import { defaultSettings } from "../default-settings.js";
import { parseBigNotifications } from "./big-notification.js";
import { listenChannel } from "./channel.js";

const cache = new WeakMap<pg.Pool, TriggerEventsFanoutMemoizer>();

export function getTriggerEventsFanoutMemoizer(
    pool: pg.Pool,
    onError: (error: unknown) => void,
) {
    let cached = cache.get(pool);
    if (cached == null) {
        cached = createTriggerEventsFanoutMemoizer(pool, onError);
        cache.set(pool, cached);
    }
    return cached;
}

export interface TriggerInsertEvent<Key extends object = any, Data extends object = any> {
    op: "INSERT";
    schema: string;
    table: string;
    key: Key;
    data: Data;
    notification: number;
}
export interface TriggerUpdateEvent<Key extends object = any, Data extends object = any> {
    op: "UPDATE";
    schema: string;
    table: string;
    key: Key;
    data: Data;
    notification: number;
}
export interface TriggerDeleteEvent<Key extends object = any> {
    op: "DELETE";
    schema: string;
    table: string;
    key: Key;
    notification: number;
}
export type TriggerEventUnion<Key extends object = any, Data extends object = any> =
    TriggerInsertEvent<Key, Data> |
    TriggerUpdateEvent<Key, Data> |
    TriggerDeleteEvent<Key>;

export type TriggerEventsFanout<Key extends object = any, Data extends object = any> =
    IterableFanout<TriggerEventUnion<Key, Data>>;

export type TriggerEventsFanoutMemoizer<Key extends object = any, Data extends object = any> =
    InstanceMemoizer<Promise<TriggerEventsFanout<Key, Data>>, [string]>;

export function createTriggerEventsFanoutMemoizer<
    Key extends object = any, Data extends object = any
>(
    pool: pg.Pool,
    onError: (error: unknown) => void,
    keepaliveInterval = defaultSettings.keepaliveInterval,
    maxChunkAge = defaultSettings.maxChunkAge,
): TriggerEventsFanoutMemoizer<Key, Data> {
    const abortControllers = new WeakMap<
        Promise<IterableFanout<TriggerEventUnion<Key, Data>>>, AbortController
    >();

    const createInstace = (
        channel: string,
    ) => {
        const abortController = new AbortController();
        const { signal } = abortController;

        const instance = createInstancePromise(channel, signal);
        abortControllers.set(instance, abortController);
        abortController.signal.addEventListener(
            "abort",
            () => abortControllers.delete(instance),
        );
        return instance;
    };

    const destroyInstance = (
        instance: Promise<IterableFanout<TriggerEventUnion<Key, Data>>>,
    ) => {
        const abortController = abortControllers.get(instance);
        assert(abortController);
        abortController.abort();
    };

    const memoizer = new InstanceMemoizer(
        createInstace,
        destroyInstance,
        channel => channel,
    );

    return memoizer;

    async function createInstancePromise(
        channel: string,
        signal: AbortSignal,
    ): Promise<TriggerEventsFanout<Key, Data>> {
        const client = await pool.connect();

        const notifications = await listenChannel(
            client,
            channel,
            keepaliveInterval,
            signal,
        );
        const bigNotifications = parseBigNotifications(
            notifications,
            maxChunkAge,
        );

        const events = {
            async *[Symbol.asyncIterator]() {
                try {
                    for await (const { key, payload } of bigNotifications) {
                        const parsedPayload = JSON.parse(payload);
                        yield {
                            ...parsedPayload,
                            notification: key,
                        };
                    }
                    client.release();
                }
                catch (error) {
                    client.release(true);

                    throw error;
                }
            },
        };

        const fanout = createIterableFanout(events);
        return fanout;
    }

}
