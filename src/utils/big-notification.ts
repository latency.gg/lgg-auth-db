import assert from "assert";
import { defaultSettings } from "../default-settings.js";

export interface BigNotification {
    key: number,
    message: string
}

interface NotificationChunk {
    timestamp: number
    value: string
}

export async function* parseBigNotifications(
    messages: AsyncIterable<string>,
    maxChunkAge = defaultSettings.maxChunkAge,
) {
    const notificationChunks = new Map<number, NotificationChunk>();

    for await (const message of messages) {
        const now = new Date().valueOf();
        const parts = message.split(/\n/g, 3);
        const notificationKey = Number(parts[0]);
        const chunksLeft = Number(parts[1]);
        const chunkPayload = parts[2];

        let notificationChunk = notificationChunks.get(notificationKey);
        if (notificationChunk != null) {
            assert(
                notificationChunk.timestamp + maxChunkAge > now,
                "chunk too old",
            );
        }

        if (chunksLeft === 0) {
            if (notificationChunk == null) {
                yield {
                    key: notificationKey,
                    payload: chunkPayload,
                };
            }
            else {
                notificationChunks.delete(notificationKey);
                yield {
                    key: notificationKey,
                    payload: notificationChunk.value + chunkPayload,
                };
            }
        }
        else {
            if (notificationChunk == null) {
                notificationChunk = {
                    value: chunkPayload,
                    timestamp: now,
                };
            }
            else {
                notificationChunk = {
                    value: notificationChunk.value + chunkPayload,
                    timestamp: now,
                };
            }

            notificationChunks.set(notificationKey, notificationChunk);
        }
    }
}

